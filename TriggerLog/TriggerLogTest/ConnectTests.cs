﻿using System.Diagnostics;
using System.Net;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using TriggerLog.DBWrappers;
using TriggerLog.DBWrappers.Common;

namespace TriggerLogTest
{
    [TestClass]
    public class ConnectionToDB
    {
        [TestMethod]
        public void Connection()
        {
            var testDatabase = new SqlServerDatabase();
            testDatabase.Connect(IPAddress.Loopback, "test", "itanchyn", "igor625745");
        }

        [TestMethod]
        public void GetAllTables()
        {
            var testDatabase = new SqlServerDatabase();
            testDatabase.Connect(IPAddress.Loopback, "test", "itanchyn", "igor625745");
            testDatabase.GetTables();
        }

        [TestMethod]
        public void CreateTriggerInsert()
        {
            var testDatabase = new SqlServerDatabase();
            testDatabase.Connect(IPAddress.Loopback, "test", "itanchyn", "igor625745");

            var table = testDatabase.GetTables()[4];
            Debug.WriteLine(testDatabase.GetTables()[4].name);
            testDatabase.CreateTrigger(table, TriggerKind.Insert);
        }


        [TestMethod]
        public void CreateTriggerDelete()
        {
            var testDatabase = new SqlServerDatabase();
            testDatabase.Connect(IPAddress.Loopback, "test", "itanchyn", "igor625745");

            var table = testDatabase.GetTables()[4];
            Debug.WriteLine(testDatabase.GetTables()[4].name);
            testDatabase.CreateTrigger(table, TriggerKind.Delete);
        }

        [TestMethod]
        public void CreateTriggerUpdate()
        {
            var testDatabase = new SqlServerDatabase();
            testDatabase.Connect(IPAddress.Loopback, "test", "itanchyn", "igor625745");

            var table = testDatabase.GetTables()[4];
            Debug.WriteLine(testDatabase.GetTables()[4].name);
            testDatabase.CreateTrigger(table, TriggerKind.Update);
        }

        [TestMethod]
        public void EnableTrigger()
        {
            var testDatabase = new SqlServerDatabase();
            testDatabase.Connect(IPAddress.Loopback, "test", "itanchyn", "igor625745");

            var table = testDatabase.GetTables()[4];
            var list = testDatabase.GetTriggers(table.name);
            var trigger = list[3];
            Debug.WriteLine(trigger.name);

            testDatabase.EnableTrigger(table, trigger);
        }

        [TestMethod]
        public void RemoveTriggerTest()
        {
            var testDatabase = new SqlServerDatabase();
            testDatabase.Connect(IPAddress.Loopback, "test", "itanchyn", "igor625745");

            var table = testDatabase.GetTables()[4];
            var list = testDatabase.GetTriggers(table.name);
            var trigger = list[0];
            Debug.WriteLine(trigger.name);

            testDatabase.RemoveTrigger(table, trigger);
        }

    }
}
