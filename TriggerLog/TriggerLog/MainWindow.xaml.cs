﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using TriggerLog.Forms;
using TriggerLog.DBWrappers;
using TriggerLog.DBWrappers.Common;

namespace TriggerLog
{
	/// <summary>
	/// Interaction logic for MainWindow.xaml
	/// </summary>
	public partial class MainWindow : Window
	{
		public Stack<FormBase> formStack = new Stack<FormBase>();
		public int animationDuration = 500;

		public MainWindow()
		{
			InitializeComponent();
			var loginForm = new LoginForm(this);
			formStack.Push(loginForm);
			grid.Children.Add(loginForm);
		}

		public void GoForwardTo(FormBase form, bool removeCurrent, bool popup = false)
		{
			var current = formStack.Peek();

			if (removeCurrent)
			{
				formStack.Pop();
				current.forward.Completed += (s, e) => grid.Children.Remove(current);
			}

			if (!popup)
				current.forward.Begin();

			formStack.Push(form);
			grid.Children.Add(form);

			form.forward.Begin();
		}

		public void GoBackward()
		{
			if (formStack.Count > 1)
			{
				var current = formStack.Pop();
				var prev = formStack.Peek();

				current.backward.Completed += (s, e) => grid.Children.Remove(current);
				current.backward.Begin();

				prev.backward.Begin();
			}
		}

		public void ClosePopup()
		{
			if (formStack.Count > 1)
			{
				var current = formStack.Pop();
				var prev = formStack.Peek();

				current.backward.Completed += (s, e) => grid.Children.Remove(current);
				current.backward.Begin();
			}
		}

		private void Window_Loaded(object sender, RoutedEventArgs e)
		{
			formStack.Peek().InitAnimations();
		}
	}
}
