﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using TriggerLog.DBWrappers.Common;

namespace TriggerLog.Forms.Elements
{
	/// <summary>
	/// Interaction logic for DbTableTile.xaml
	/// </summary>
	public partial class DbTableTile : UserControl
	{
		private IDatabase db;
		private Table table;
		private TableListForm parentForm;

		public DbTableTile(IDatabase database, Table table, TableListForm tableList)
		{
			this.db = database;
			this.table = table;
			this.parentForm = tableList;

			InitializeComponent();

			tableNameBox.Text = table.name;
			
			foreach (var field in table.fields)
			{
				var fieldLabel = new TextBlock();
				fieldLabel.Text = field;

				fieldListPanel.Children.Add(fieldLabel);
			}

			if (table.triggers != null)
			{
				foreach (var trigger in table.triggers)
				{
					switch (trigger.kind)
					{
						case TriggerKind.Update:
							if (trigger.isEnabled)
								updateTriggerIndicator.Foreground = (Brush)Resources["TriggerActive"];
							else
								updateTriggerIndicator.Foreground = (Brush)Resources["TriggerDisabled"];
							break;

						case TriggerKind.Insert:
							if (trigger.isEnabled)
								insertTriggerIndicator.Foreground = (Brush)Resources["TriggerActive"];
							else
								insertTriggerIndicator.Foreground = (Brush)Resources["TriggerDisabled"];
							break;

						case TriggerKind.Delete:
							if (trigger.isEnabled)
								deleteTriggerIndicator.Foreground = (Brush)Resources["TriggerActive"];
							else
								deleteTriggerIndicator.Foreground = (Brush)Resources["TriggerDisabled"];
							break;
					}
				}
			}
		}

		private void Button_Click(object sender, RoutedEventArgs e)
		{
			var log = new TableLogForm(db, table, parentForm);
			log.InitAnimations();
			parentForm.window.GoForwardTo(log, false, true);
		}
	}
}
