﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using TriggerLog.DBWrappers.Common;
using System.IO;
using Microsoft.Win32;

namespace TriggerLog.Forms
{
	/// <summary>
	/// Interaction logic for TableLogForm.xaml
	/// </summary>
	public partial class TableLogForm : FormBase
	{
		private IDatabase db;
		private Table table;
		private TableListForm parentForm;

		public TableLogForm(IDatabase database, Table table, TableListForm tableList)
		{
			this.db = database;
			this.table = table;
			this.parentForm = tableList;
			InitializeComponent();

			tableNameBox.Text = table.name;

			UpdateState();
		}

		private void UpdateState()
		{
			if (table.triggers != null)
			{
				foreach (var trigger in table.triggers)
				{
					switch (trigger.kind)
					{
						case TriggerKind.Update:
							updateLog.ItemsSource = db.GetLogEntries(trigger);
							updateTriggerIndicator.Tag = trigger;

							if (trigger.isEnabled)
								updateTriggerIndicator.Foreground = (Brush)Resources["TriggerActive"];
							else
								updateTriggerIndicator.Foreground = (Brush)Resources["TriggerDisabled"];
							break;

						case TriggerKind.Insert:
							insertLog.ItemsSource = db.GetLogEntries(trigger);
							insertTriggerIndicator.Tag = trigger;

							if (trigger.isEnabled)
								insertTriggerIndicator.Foreground = (Brush)Resources["TriggerActive"];
							else
								insertTriggerIndicator.Foreground = (Brush)Resources["TriggerDisabled"];
							break;

						case TriggerKind.Delete:
							deleteLog.ItemsSource = db.GetLogEntries(trigger);
							deleteTriggerIndicator.Tag = trigger;

							if (trigger.isEnabled)
								deleteTriggerIndicator.Foreground = (Brush)Resources["TriggerActive"];
							else
								deleteTriggerIndicator.Foreground = (Brush)Resources["TriggerDisabled"];
							break;
					}
				}
			}
		}

		private void updateTriggerIndicator_Click(object sender, RoutedEventArgs e)
		{
			var trigger = updateTriggerIndicator.Tag as DBWrappers.Common.Trigger;

			if (trigger != null)
			{
				if (trigger.isEnabled)
					db.DisableTrigger(table, trigger);
				else
					db.EnableTrigger(table, trigger);
			}
			else
				db.CreateTrigger(table, TriggerKind.Update);

			UpdateState();
		}

		private void insertTriggerIndicator_Click(object sender, RoutedEventArgs e)
		{
			var trigger = insertTriggerIndicator.Tag as DBWrappers.Common.Trigger;

		    if (trigger != null)
		    {
		        if (trigger.isEnabled)
		            db.DisableTrigger(table, trigger);
		        else
		            db.EnableTrigger(table, trigger);
		    }
		    else
		        db.CreateTrigger(table, TriggerKind.Insert);

			UpdateState();
		}

		private void deleteTriggerIndicator_Click(object sender, RoutedEventArgs e)
		{
			var trigger = deleteTriggerIndicator.Tag as DBWrappers.Common.Trigger;

			if (trigger != null)
			{
				if (trigger.isEnabled)
					db.DisableTrigger(table, trigger);
				else
					db.EnableTrigger(table, trigger);
			}
			else
				db.CreateTrigger(table, TriggerKind.Delete);

			UpdateState();
		}

		public override void InitAnimations()
		{
			forward = new Storyboard();

			DoubleAnimation fadeIn = new DoubleAnimation();
			fadeIn.From = 0;
			fadeIn.To = 1;
			fadeIn.Duration = new Duration(TimeSpan.FromMilliseconds(parentForm.window.animationDuration));
			fadeIn.EasingFunction = new HermiteEasing();
			
			Storyboard.SetTarget(fadeIn, this);
			Storyboard.SetTargetProperty(fadeIn, new PropertyPath("Opacity"));
			forward.Children.Add(fadeIn);

			backward = new Storyboard();

			DoubleAnimation fadeOut = new DoubleAnimation();
			fadeOut.From = 1;
			fadeOut.To = 0;
			fadeOut.Duration = new Duration(TimeSpan.FromMilliseconds(parentForm.window.animationDuration));
			fadeOut.EasingFunction = new HermiteEasing();

			Storyboard.SetTarget(fadeOut, this);
			Storyboard.SetTargetProperty(fadeOut, new PropertyPath("Opacity"));
			backward.Children.Add(fadeOut);
		}

		private void closeButton_Click(object sender, RoutedEventArgs e)
		{
			parentForm.window.ClosePopup();
			parentForm.Update();
		}

		private void button_Click(object sender, RoutedEventArgs e)
		{
			SaveFileDialog dialog = new SaveFileDialog();

			dialog.DefaultExt = ".csv";

			var result = dialog.ShowDialog(parentForm.window);
			if (result.HasValue && result.Value)
			{
				var stream = new StreamWriter(dialog.OpenFile());
				List<LogEntry> log = null;

				switch (tabControl.SelectedIndex)
				{
					case 0:
						log = db.GetLogEntries(updateTriggerIndicator.Tag as DBWrappers.Common.Trigger);
						break;
					case 1:
						log = db.GetLogEntries(insertTriggerIndicator.Tag as DBWrappers.Common.Trigger);
						break;
					case 2:
						log = db.GetLogEntries(deleteTriggerIndicator.Tag as DBWrappers.Common.Trigger);
						break;
				}

				for (int i = 0; i < log.Count; i++)
				{
					string row = log[i].userName + ";" + log[i].dateTime + ";" + log[i].oldValue + ";" + log[i].newValue + "\n";

					stream.Write(row);
				}

				stream.Flush();
				stream.Close();
			}
		}
	}
}
