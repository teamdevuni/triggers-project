﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Media;
using System.Windows.Media.Animation;

namespace TriggerLog.Forms
{
	class HermiteEasing : IEasingFunction
	{
		public double Ease(double t)
		{
			return -2 * t * t * t + 3 * t * t;
		}
	}

	public abstract class FormBase : UserControl
	{
		//public Storyboard slideToRight = new Storyboard();
		//public Storyboard slideToLeft = new Storyboard();

		public Storyboard forward;
		public Storyboard backward;

		public FormBase()
		{
			/*
			RenderTransform = new TranslateTransform();

			DoubleAnimation animation = new DoubleAnimation();
			animation.By = -500;
			animation.Duration = new Duration(TimeSpan.FromMilliseconds(300));

			animation.EasingFunction = new HermiteEasing();

			Storyboard.SetTarget(animation, this);
			Storyboard.SetTargetProperty(animation, new PropertyPath("RenderTransform.(TranslateTransform.X)"));

			slideToLeft.Children.Add(animation);

			animation = new DoubleAnimation();
			animation.By = 500;
			animation.Duration = new Duration(TimeSpan.FromMilliseconds(300));

			animation.EasingFunction = new HermiteEasing();

			Storyboard.SetTarget(animation, this);
			Storyboard.SetTargetProperty(animation, new PropertyPath("RenderTransform.(TranslateTransform.X)"));

			slideToRight.Children.Add(animation);*/
		}

		public abstract void InitAnimations();
	}
}
