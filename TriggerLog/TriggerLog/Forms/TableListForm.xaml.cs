﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using TriggerLog.DBWrappers.Common;
using TriggerLog.Forms.Elements;

namespace TriggerLog.Forms
{
	/// <summary>
	/// Interaction logic for TableListForm.xaml
	/// </summary>
	public partial class TableListForm : FormBase
	{
		public MainWindow window;
		private IDatabase database;

		public TableListForm(MainWindow window, IDatabase database)
		{
			this.window = window;
			this.database = database;

			InitializeComponent();

			dbNameText.Text = database.GetDisplayableName();
			Update();
		}

		public override void InitAnimations()
		{
			RenderTransform = new TranslateTransform();

			forward = new Storyboard();

			DoubleAnimation goLeft = new DoubleAnimation();
			goLeft.By = -Width;
			goLeft.Duration = new Duration(TimeSpan.FromMilliseconds(window.animationDuration));
			goLeft.EasingFunction = new HermiteEasing();

			Storyboard.SetTarget(goLeft, this);
			Storyboard.SetTargetProperty(goLeft, new PropertyPath("RenderTransform.(TranslateTransform.X)"));

			forward.Children.Add(goLeft);

			backward = new Storyboard();

			DoubleAnimation goRight = new DoubleAnimation();
			goRight.By = Width;
			goRight.Duration = new Duration(TimeSpan.FromMilliseconds(window.animationDuration));
			goRight.EasingFunction = new HermiteEasing();

			Storyboard.SetTarget(goRight, this);
			Storyboard.SetTargetProperty(goRight, new PropertyPath("RenderTransform.(TranslateTransform.X)"));

			backward.Children.Add(goRight);
		}

		public void Update()
		{
			tableListPanel.Children.Clear();

			var tableList = database.GetTables();

			foreach (var table in tableList)
			{
				var tableTile = new DbTableTile(database, table, this);
				tableListPanel.Children.Add(tableTile);
			}
		}
	}
}
