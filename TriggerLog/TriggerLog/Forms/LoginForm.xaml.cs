﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Media.Animation;
using System.Windows.Navigation;
using System.Windows.Shapes;
using TriggerLog.DBWrappers;
using System.Net;
using TriggerLog.DBWrappers.Common;

namespace TriggerLog.Forms
{
	/// <summary>
	/// Interaction logic for LoginForm.xaml
	/// </summary>
	public partial class LoginForm : FormBase
	{
		private MainWindow window;

        //Style
        //ip
        private void gotfocus_ip(object sender, RoutedEventArgs e)
        {
            watermarked_ipBox.Visibility = System.Windows.Visibility.Collapsed;
            ipBox.Visibility = System.Windows.Visibility.Visible;
            ipBox.Focus();
        }

        private void userInput_ip(object sender, RoutedEventArgs e)
        {
            if (string.IsNullOrEmpty(ipBox.Text))
            {
                ipBox.Visibility = System.Windows.Visibility.Collapsed;
                watermarked_ipBox.Visibility = System.Windows.Visibility.Visible;
            }
        }
        //db
        private void gotfocus_db(object sender, RoutedEventArgs e)
        {
            watermarked_dbBox.Visibility = System.Windows.Visibility.Collapsed;
            dbBox.Visibility = System.Windows.Visibility.Visible;
            dbBox.Focus();
            
        }

        private void userInput_db(object sender, RoutedEventArgs e)
        {
            if (string.IsNullOrEmpty(dbBox.Text))
            {
                dbBox.Visibility = System.Windows.Visibility.Collapsed;
                watermarked_dbBox.Visibility = System.Windows.Visibility.Visible;
            }
        }
        //user
        private void gotfocus_user(object sender, RoutedEventArgs e)
        {
            watermarked_userBox.Visibility = System.Windows.Visibility.Collapsed;
            userBox.Visibility = System.Windows.Visibility.Visible;
            userBox.Focus();
        }

        private void userInput_user(object sender, RoutedEventArgs e)
        {
            if (string.IsNullOrEmpty(userBox.Text))
            {
                userBox.Visibility = System.Windows.Visibility.Collapsed;
                watermarked_userBox.Visibility = System.Windows.Visibility.Visible;
            }
        }
        //pswd
        private void gotfocus_pswd(object sender, RoutedEventArgs e)
        {
            watermarked_passwordBox.Visibility = System.Windows.Visibility.Collapsed;
            passwordBox.Visibility = System.Windows.Visibility.Visible;
            passwordBox.Focus();
        }

        private void userInput_pswd(object sender, RoutedEventArgs e)
        {
            if (string.IsNullOrEmpty(passwordBox.Text))
            {
                passwordBox.Visibility = System.Windows.Visibility.Collapsed;
                watermarked_passwordBox.Visibility = System.Windows.Visibility.Visible;
            }
        }

        //End Style

		public LoginForm(MainWindow window)
		{
			this.window = window;
			InitializeComponent();
		}

		public override void InitAnimations()
		{
			RenderTransform = new TranslateTransform();

			window.ResizeMode = ResizeMode.CanMinimize;

			forward = new Storyboard();

			DoubleAnimation goLeft = new DoubleAnimation();
			goLeft.By = -Width;
			goLeft.Duration = new Duration(TimeSpan.FromMilliseconds(window.animationDuration));
			goLeft.EasingFunction = new HermiteEasing();
			Storyboard.SetTarget(goLeft, this);
			Storyboard.SetTargetProperty(goLeft, new PropertyPath("RenderTransform.(TranslateTransform.X)"));
			forward.Children.Add(goLeft);

			Storyboard test = new Storyboard();
			forward.Completed += (s, e) => test.Begin();
			
			DoubleAnimation windowX = new DoubleAnimation();
			windowX.To = 800 + (window.Width - Width);
			windowX.Duration = new Duration(TimeSpan.FromMilliseconds(window.animationDuration));
			windowX.EasingFunction = new HermiteEasing();
			Storyboard.SetTarget(windowX, window);
			Storyboard.SetTargetProperty(windowX, new PropertyPath("Width"));
			test.Children.Add(windowX);

			DoubleAnimation windowY = new DoubleAnimation();
			windowY.To = 600 + (window.Height - Height);
			windowY.Duration = new Duration(TimeSpan.FromMilliseconds(window.animationDuration));
			windowY.EasingFunction = new HermiteEasing();
			Storyboard.SetTarget(windowY, window);
			Storyboard.SetTargetProperty(windowY, new PropertyPath("Height"));
			test.Children.Add(windowY);

			backward = new Storyboard();

			DoubleAnimation goRight = new DoubleAnimation();
			goRight.By = Width;
			goRight.Duration = new Duration(TimeSpan.FromMilliseconds(window.animationDuration));
			goRight.EasingFunction = new HermiteEasing();

			Storyboard.SetTarget(goRight, this);
			Storyboard.SetTargetProperty(goRight, new PropertyPath("RenderTransform.(TranslateTransform.X)"));

			backward.Children.Add(goRight);
		}

		private void connectButton_Click(object sender, RoutedEventArgs e)
		{
			IDatabase db;

			if (dbBox.Text == "fake")
				db = new FakeDatabase();
			else
				db = new SqlServerDatabase();

			IPAddress ip = IPAddress.None;

			string ipString = ipBox.Text;
			if (ipString == "")
				ipString = "127.0.0.1";

			if (IPAddress.TryParse(ipString, out ip))
			{
				try
				{
					db.Connect(ip, dbBox.Text, userBox.Text, passwordBox.Text);

					var tableListForm = new TableListForm(window, db);
					tableListForm.Width = Width;
					tableListForm.InitAnimations();
					tableListForm.Width = double.NaN;
					tableListForm.RenderTransform = new TranslateTransform(Width, 0);
					window.SizeToContent = SizeToContent.Manual;

					window.GoForwardTo(tableListForm, true);
					window.ResizeMode = ResizeMode.CanResize;
				}
				catch (Exception exception)
				{
					MessageBox.Show(window, "Can't connect to specified database.\nReason:\n" + exception.Message, "Error");
				}
			}
			else
				MessageBox.Show(window, "Can't parse ip address.");
		}
	}
}