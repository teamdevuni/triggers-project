﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using TriggerLog.DBWrappers.Common;

namespace TriggerLog.DBWrappers
{
	class FakeDatabase : IDatabase
	{
		private List<Table> tables;

		public FakeDatabase()
		{
			tables = new List<Table>();

			var updateTrigger1 = new Trigger() { kind = TriggerKind.Update, name = "triggerlog_update_table1", isEnabled = true, logTableName = "triggerlog_update_table1_log" };
			var updateTrigger2 = new Trigger() { kind = TriggerKind.Update, name = "triggerlog_update_table2", isEnabled = false, logTableName = "triggerlog_update_table2_log" };

			var insertTrigger = new Trigger() { kind = TriggerKind.Insert, name = "triggerlog_insert_table1", isEnabled = false, logTableName = "triggerlog_insert_table1_log" };

			var deleteTrigger = new Trigger() { kind = TriggerKind.Delete, name = "triggerlog_delete_table2", isEnabled = true, logTableName = "triggerlog_delete_table2_log" };

			tables.Add(new Table() { name = "table1", fields = new List<string>() { "field1", "field2", "field3" }, triggers = new List<Trigger>() { updateTrigger1, insertTrigger } });
			tables.Add(new Table() { name = "table2", fields = new List<string>() { "field1", "field2", "field3", "field4" }, triggers = new List<Trigger>() { updateTrigger2, deleteTrigger } });
		}

		public void Connect(IPAddress address, string dbName, string login, string password)
		{

		}

		public string GetDisplayableName()
		{
			return "Fake database";
		}

		public List<Table> GetTables()
		{
			return tables;
		}
		
		public void CreateTrigger(Table table, TriggerKind kind)
		{
			var trigger = new Trigger() { kind = kind, name = "triggerlog_" + kind.ToString() + "_" + table.name, isEnabled = true, logTableName = "triggerlog_" + kind.ToString() + "_" + table.name + "_log" };
			table.triggers.Add(trigger);
		}

		public void EnableTrigger(Table table, Trigger trigger)
		{
			trigger.isEnabled = true;
		}

		public void RemoveTrigger(Table table, Trigger trigger)
		{
			table.triggers.Remove(trigger);
		}

		public void DisableTrigger(Table table, Trigger trigger)
		{
			trigger.isEnabled = false;
		}

		public List<LogEntry> GetLogEntries(Trigger trigger, int skipFirstEntries = 0, int entriesCount = 1000)
		{
			List<LogEntry> log = new List<LogEntry>();

			switch (trigger.kind)
			{
				case TriggerKind.Update:
					log.Add(new LogEntry() { userName = "asda", dateTime = DateTime.Now, newValue = "asdasd", oldValue = "asdafdsf" });
					log.Add(new LogEntry() { userName = "asda", dateTime = DateTime.Now, newValue = "asdasd", oldValue = "asdafdsf" });
					log.Add(new LogEntry() { userName = "asda", dateTime = DateTime.Now, newValue = "asdasd", oldValue = "asdafdsf" });
					log.Add(new LogEntry() { userName = "asda", dateTime = DateTime.Now, newValue = "asdasd", oldValue = "asdafdsf" });
					log.Add(new LogEntry() { userName = "asda", dateTime = DateTime.Now, newValue = "asdasd", oldValue = "asdafdsf" });
					break;
				case TriggerKind.Insert:
					log.Add(new LogEntry() { userName = "asda", dateTime = DateTime.Now, newValue = "asdasd" });
					log.Add(new LogEntry() { userName = "asda", dateTime = DateTime.Now, newValue = "asdasd" });
					log.Add(new LogEntry() { userName = "asda", dateTime = DateTime.Now, newValue = "asdasd" });
					log.Add(new LogEntry() { userName = "asda", dateTime = DateTime.Now, newValue = "asdasd" });
					log.Add(new LogEntry() { userName = "asda", dateTime = DateTime.Now, newValue = "asdasd" });
					break;
				case TriggerKind.Delete:
					log.Add(new LogEntry() { userName = "asda", dateTime = DateTime.Now, oldValue = "asdafdsf" });
					log.Add(new LogEntry() { userName = "asda", dateTime = DateTime.Now, oldValue = "asdafdsf" });
					log.Add(new LogEntry() { userName = "asda", dateTime = DateTime.Now, oldValue = "asdafdsf" });
					log.Add(new LogEntry() { userName = "asda", dateTime = DateTime.Now, oldValue = "asdafdsf" });
					log.Add(new LogEntry() { userName = "asda", dateTime = DateTime.Now, oldValue = "asdafdsf" });
					break;
			}

			return log;
		}
	}
}
