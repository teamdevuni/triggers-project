﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TriggerLog.DBWrappers.Common
{
	public class Table
	{
		public string name;
		public List<string> fields;
		public List<Trigger> triggers;
	}
}
