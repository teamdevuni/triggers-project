﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TriggerLog.DBWrappers.Common
{
	public class LogEntry
	{
		public string userName { get; set; }
		public DateTime dateTime { get; set; }

		//NOTE(yura): For trigger on:
		//UPDATE: both old and new values are used
		//INSERT: only new value is used
		//DELETE: only old value is used
		public string oldValue { get; set; }
		public string newValue { get; set; }
	}
}
