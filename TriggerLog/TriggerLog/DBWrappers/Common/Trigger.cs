﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TriggerLog.DBWrappers.Common
{
	public enum TriggerKind
    {
		Update,
		Insert,
		Delete,
	}
	
	public class Trigger
	{
		public string name;
		public string logTableName;
		public bool isEnabled;
		public TriggerKind kind;
	}
}
