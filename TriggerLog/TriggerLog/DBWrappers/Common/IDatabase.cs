﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;

namespace TriggerLog.DBWrappers.Common
{
	public interface IDatabase
	{
		void Connect(IPAddress address, string dbName, string login, string password);
		string GetDisplayableName();
		List<Table> GetTables();
		void CreateTrigger(Table table, TriggerKind kind);
		void EnableTrigger(Table table, Trigger trigger);
		void RemoveTrigger(Table table, Trigger trigger);
		void DisableTrigger(Table table, Trigger trigger);
		List<LogEntry> GetLogEntries(Trigger trigger, int skipFirstEntries = 0, int entriesCount = 1000);
	}
}
