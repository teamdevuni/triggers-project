﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Net;
using TriggerLog.DBWrappers.Common;
using System.Data.SqlClient;
using System.Diagnostics;
using System.Linq;

namespace TriggerLog.DBWrappers
{
    public class SqlServerDatabase : IDatabase
    {
        private string DbData;
        private SqlConnection Connection { get; set; }
        public void Connect(IPAddress address, string dbName, string login, string password)
        {
            Connection = new SqlConnection(String.Format("Data Source={0},1433; Network Library=DBMSSOCN; Initial Catalog={1}; User Id={2}; Password={3};", address, dbName, login, password));
            DbData = string.Format("IP: {0} DB name: {1} User name: {2}", address, dbName, login);  

            try
            {
                Connection.Open();
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                throw;
            }
        }

        public List<Table> GetTables()
        {
            if (Connection.State == ConnectionState.Closed) Connection.Open();

            DataTable tableSchema = Connection.GetSchema("Tables");
            List<Table> Tables = new List<Table>();

            foreach (DataRow row in tableSchema.Rows)
            {
                var dataTable = new DataTable();
                var tableName = row[2].ToString();

                if (!tableName.StartsWith("TriggerLog_"))
                {
                    SqlCommand command = Connection.CreateCommand();
                    command.CommandText = "SELECT COLUMN_NAME " +
                                          "FROM INFORMATION_SCHEMA.COLUMNS " +
                                          "WHERE TABLE_NAME = '" + tableName + "'";

                    dataTable.Load(command.ExecuteReader());
                    var fields = dataTable.Rows.Cast<DataRow>().ToList().Select(r => r[0].ToString()).ToList();

                    Tables.Add(new Table() {name = tableName, fields = fields, triggers = GetTriggers(tableName)});
                }
            }

            return Tables;
        }

        public List<Trigger> GetTriggers(string tableName)
        {
            if (Connection.State == ConnectionState.Closed) Connection.Open();

            SqlCommand command = Connection.CreateCommand();
            command.CommandText = "SELECT * FROM sys.triggers";

            var dataTable = new DataTable();
            dataTable.Load(command.ExecuteReader());

            List<Trigger> Triggers = new List<Trigger>();

            foreach (DataRow row in dataTable.Rows)
            {
                var trgName = row.ItemArray[0].ToString();
                TriggerKind trgKind;
                Enum.TryParse(trgName.Split('_')[1], out trgKind);

                Debug.WriteLine(trgName.Split('_')[2]);

                if (trgName.StartsWith("Trigger_") && trgName.Split('_')[2] == tableName)
                    Triggers.Add(new Trigger()
                    {
                        name = trgName,
                        kind = trgKind,
                        isEnabled = !(bool)row.ItemArray[10],
                        logTableName = "TriggerLog_" + trgName.Split('_')[2] + "_" + trgKind.ToString()
                    });
            }
            
            Debug.WriteLine(Triggers.Count);
            return Triggers;
        }

        public void CreateTrigger(Table table, TriggerKind kind)
		{
            if (Connection.State == ConnectionState.Closed) Connection.Open();

            SqlCommand command  = Connection.CreateCommand();
            string logTableName = string.Format("TriggerLog_{0}_{1}", table.name, kind);
		    string triggerName  = string.Format("Trigger_{0}_{1}", kind, table.name);

		    command.CommandText = string.Format("IF NOT EXISTS (SELECT * FROM sys.tables WHERE name = '{0}') CREATE TABLE {0} (" + 
                                                "ID INT IDENTITY(1, 1) PRIMARY KEY, UserName VARCHAR(255) NOT NULL, ChangeDate DATETIME NOT NULL, OldValue NVARCHAR(MAX), NewValue NVARCHAR(MAX));", logTableName);
		    command.ExecuteNonQuery();

		    command = Connection.CreateCommand();
            command.CommandText = string.Format("CREATE TRIGGER {0} ON {1} AFTER {2} AS BEGIN ", triggerName, table.name, kind);

		    if (kind == TriggerKind.Insert)
		    {
		        string insertData = string.Format("INSERT INTO {0}(UserName, ChangeDate, OldValue, NewValue) VALUES(SUSER_SNAME(), GETDATE(), NULL, CONCAT(", logTableName);
		        for (int i = 0; i < table.fields.Count; i++)
		        { 
                    insertData += string.Format("(SELECT CONCAT({0}, '; ') FROM inserted)", table.fields[i]);
		            if (i != table.fields.Count - 1) insertData += ", ";
		            else insertData += "))";
		        }
                Debug.WriteLine("Insert Data: " + insertData);
		        command.CommandText += insertData + "END";
                Debug.WriteLine("Create trigger command:" + command.CommandText);

		        command.ExecuteNonQuery();
		    }
            else if (kind == TriggerKind.Update)
		    {
		        string newValue = "CONCAT(";
		        string oldValue = "CONCAT(";
                string updateData = string.Format("INSERT INTO {0}(UserName, ChangeDate, OldValue, NewValue) VALUES(SUSER_SNAME(), GETDATE(), ", logTableName);
                for (int i = 0; i < table.fields.Count; i++)
                {
                    newValue += string.Format("(SELECT CONCAT({0}, '; ') FROM inserted)", table.fields[i]);
                    oldValue += string.Format("(SELECT CONCAT({0}, '; ') FROM deleted)", table.fields[i]);
                    if (i != table.fields.Count - 1)
                    {
                        newValue += ", ";
                        oldValue += ", ";
                    }
                    else
                    {
                        newValue += ")";
                        oldValue += ")";
                    }
                }
		        updateData += oldValue + ", " + newValue + ")";
                Debug.WriteLine("Insert Data: " + updateData);
                command.CommandText += updateData + "END";
                Debug.WriteLine("Create trigger command:" + command.CommandText);

                command.ExecuteNonQuery();

            }
            else if (kind == TriggerKind.Delete)
		    {
                string deletedData = string.Format("INSERT INTO {0}(UserName, ChangeDate, OldValue, NewValue) VALUES(SUSER_SNAME(), GETDATE(), CONCAT(", logTableName);
                for (int i = 0; i < table.fields.Count; i++)
                {
                    deletedData += string.Format("(SELECT CONCAT({0}, '; ') FROM deleted)", table.fields[i]);
                    if (i != table.fields.Count - 1) deletedData += ", ";
                    else deletedData += "), NULL)";
                }
                Debug.WriteLine("Insert Data: " + deletedData);
                command.CommandText += deletedData + " END";
                Debug.WriteLine("Create trigger command:" + command.CommandText);

                command.ExecuteNonQuery();
            }

            table.triggers.Add(new Trigger() { isEnabled = true, kind = kind, logTableName = logTableName, name = triggerName});
        }

        public void EnableTrigger(Table table, Trigger trigger)
        {
            SqlCommand command = Connection.CreateCommand();
            command.CommandText = string.Format("ENABLE TRIGGER {0} ON {1}", trigger.name, table.name);
            trigger.isEnabled = true;

            command.ExecuteNonQuery();
        }

        public void RemoveTrigger(Table table, Trigger trigger)
        {
            SqlCommand command = Connection.CreateCommand();
            command.CommandText = string.Format("IF EXISTS(SELECT * FROM sys.triggers WHERE name = '{0}') DROP TRIGGER {0}", trigger.name);

            table.triggers.Remove(trigger);
            command.ExecuteNonQuery();

            command.CommandText = string.Format("IF EXISTS(SELECT * FROM sys.tables WHERE name = '{0}') DROP TABLE {0}", trigger.logTableName);
            command.ExecuteNonQuery();
        }

        public void DisableTrigger(Table table, Trigger trigger)
        {
            SqlCommand command = Connection.CreateCommand();
            command.CommandText = string.Format("DISABLE TRIGGER {0} ON {1}", trigger.name, table.name);
            trigger.isEnabled = false;

            command.ExecuteNonQuery();
        }

        public List<LogEntry> GetLogEntries(Trigger trigger, int skipFirstEntries = 0, int entriesCount = 1000)
        {
            List<LogEntry> logs = new List<LogEntry>();

            SqlCommand command = Connection.CreateCommand();

            command.CommandText = "SELECT * FROM " + trigger.logTableName;
            DataTable dataTable = new DataTable();
            dataTable.Load(command.ExecuteReader());

            foreach (DataRow row in dataTable.Rows)
            {
                logs.Add( new LogEntry() {userName = row[1].ToString(), dateTime = (DateTime)row[2], oldValue = row[3].ToString(), newValue = row[4].ToString()});
            }

            return logs;
        }

        public string GetDisplayableName()
        {
            return DbData;
        }
    }
}